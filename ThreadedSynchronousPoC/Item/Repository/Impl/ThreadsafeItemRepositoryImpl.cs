﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThreadedSynchronousPoC.Item.Model;
using ThreadedSynchronousPoC.Repository.Impl;
using ThreadedSynchronousPoC.Repository.Interface;
using ThreadedSynchronousPoC.ThreadSync;

namespace ThreadedSynchronousPoC.Item.Repository.Impl {
    class ThreadsafeItemRepositoryImpl : ItemRepository {
        private readonly ItemRepositoryImpl repo = new ItemRepositoryImpl();
        private readonly ThreadExecuter threadExecuter;

        public ThreadsafeItemRepositoryImpl(ThreadExecuter threadExecuter) {
            this.threadExecuter = threadExecuter;
        }

        public ItemDto Get(int id) {
            Func<ItemDto> get0 = () => repo.Get(id);
            return threadExecuter.Execute(get0);
        }

        public List<ItemDto> ListAll() {
            Func<List<ItemDto>> list = () => repo.ListAll();
            return threadExecuter.Execute(list);
        }
        public void Delete(ItemDto item) {
            Action list = () => repo.Delete(item);
            threadExecuter.Execute(list);
        }
    }
}
