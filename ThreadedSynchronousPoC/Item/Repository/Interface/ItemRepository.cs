﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThreadedSynchronousPoC.Item.Model;

namespace ThreadedSynchronousPoC.Repository.Interface {
    interface ItemRepository {
        List<ItemDto> ListAll();
        ItemDto Get(int id);


        void Delete(ItemDto item);
    }
}
