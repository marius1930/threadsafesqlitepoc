﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ThreadedSynchronousPoC.Item.Model;
using ThreadedSynchronousPoC.Repository.Impl;
using ThreadedSynchronousPoC.ThreadSync.Model;

namespace ThreadedSynchronousPoC.ThreadSync {
    class ThreadExecuter : IDisposable {
        private ConcurrentDictionary<AutoResetEvent, object> results = new ConcurrentDictionary<AutoResetEvent, object>();
        private ConcurrentQueue<QueuedExecution> queue = new ConcurrentQueue<QueuedExecution>();
        private Thread thread;
        private volatile bool isCancelled;

        public ThreadExecuter() {
            isCancelled = false;
            thread = new Thread(new ThreadStart(Run));
            thread.Start();

            while (!thread.IsAlive);
        }

        private void Run() {
            while (!isCancelled) {
                QueuedExecution elem;
                if (queue.TryDequeue(out elem)) {
                    try {
                        if (elem.func != null)
                            results[elem.Trigger] = elem.func();
                        else
                            elem.action();
                    }
                    catch (Exception ex) {
                        results[elem.Trigger] = ex;
                    }
                    elem.Trigger.Set();
                }
            }
        }


        public void Execute(Action func) {
            if (thread == null)
                throw new InvalidOperationException();
            AutoResetEvent ev = new AutoResetEvent(false);

            queue.Enqueue(new QueuedExecution {
                action = () => func(),
                Trigger = ev
            });

            if (!ev.WaitOne(5000, true)) {
                throw new Exception("Operation never terminated");
            }

            if (results.ContainsKey(ev)) {
                // TODO: Inner exception?
                throw (results[ev] as Exception);
            }
        }

        public T Execute<T>(Func<T> func) {
            if (thread == null)
                throw new InvalidOperationException();
            AutoResetEvent ev = new AutoResetEvent(false);
            
            queue.Enqueue(new QueuedExecution {
                func = () => func(),
                Trigger = ev
            });

            if (!ev.WaitOne(5000, true)) {
                throw new Exception("Operation never terminated");
            }

            Exception ex = results[ev] as Exception;
            if (ex != null) {
                // TODO: Inner exception?
                throw ex;
            }

            return (T)results[ev];
        }

        ~ThreadExecuter() {
            Dispose();
        }

        public void Dispose() {
            isCancelled = true;
            thread = null;
        }
    }
}
