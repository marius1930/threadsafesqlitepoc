﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThreadedSynchronousPoC.Item.Model;
using ThreadedSynchronousPoC.Item.Repository.Impl;
using ThreadedSynchronousPoC.Repository.Interface;
using ThreadedSynchronousPoC.ThreadSync;

namespace ThreadedSynchronousPoC {
    public partial class Form1 : Form {
        private ItemRepository itemRepo;
        private ThreadExecuter executer;
        public Form1() {
            InitializeComponent();
            executer = new ThreadExecuter();
            itemRepo = new ThreadsafeItemRepositoryImpl(executer);
        }

        private void button1_Click(object sender, EventArgs e) {
            Console.WriteLine($"Executing ListAll from thread {Thread.CurrentThread.ManagedThreadId}");
            MessageBox.Show(String.Join("\n", itemRepo.ListAll().Select(item => $"Item: {item.Id}")));
        }

        private void Form1_Load(object sender, EventArgs e) {
            this.FormClosing += (_, __) => executer.Dispose();
        }

        private async void button2_Click(object sender, EventArgs e) {
            Console.WriteLine($"Calling ListAll via a Task on thread {Thread.CurrentThread.ManagedThreadId}");
            var items = await Task.Run(() => {
                Console.WriteLine($"Executing ListAll from thread {Thread.CurrentThread.ManagedThreadId}");
                return itemRepo.ListAll();
            });
            MessageBox.Show(String.Join("\n", items.Select(item => $"Item: {item.Id}")));
        }

        private void button3_Click(object sender, EventArgs e) {
            Console.WriteLine($"Executing Get from thread {Thread.CurrentThread.ManagedThreadId}");
            MessageBox.Show("Item: " + itemRepo.Get(123).Id);

        }

        private void button4_Click(object sender, EventArgs e) {
            itemRepo.Delete(new ItemDto());
        }
    }
}
