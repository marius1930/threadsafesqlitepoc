﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadedSynchronousPoC.ThreadSync.Model {
    class QueuedExecution {
        public Func<object> func { get; set; }
        public Action action { get; set; }
        public AutoResetEvent Trigger { get; set; }
    }
}
