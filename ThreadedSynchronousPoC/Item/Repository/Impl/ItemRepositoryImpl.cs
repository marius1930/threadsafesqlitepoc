﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ThreadedSynchronousPoC.Item.Model;
using ThreadedSynchronousPoC.Repository.Interface;

namespace ThreadedSynchronousPoC.Repository.Impl {
    class ItemRepositoryImpl : ItemRepository {
        // This would be the real session object, connection to sqlite
        int sessionThreadId = -1;

        private void threadAssert(String f) {
            Console.WriteLine($"{f} called on thread {Thread.CurrentThread.ManagedThreadId}");
            if (sessionThreadId == -1)
                sessionThreadId = Thread.CurrentThread.ManagedThreadId;

            Debug.Assert(sessionThreadId == Thread.CurrentThread.ManagedThreadId);
        }

        public ItemDto Get(int id) {
            threadAssert("Get()");
            Thread.Sleep(8000); // Simulate a slow call, over the 5000 limit
            return new ItemDto { Id = id };
        }

        public void Delete(ItemDto item) {
            threadAssert("Delete()");
            throw new NotImplementedException();
        }

        public List<ItemDto> ListAll() {
            threadAssert("ListAll()");

            return new List<ItemDto>(new[] {
                new ItemDto { Id = 0 },
                new ItemDto { Id = 1 }
            });
        }
    }
}
